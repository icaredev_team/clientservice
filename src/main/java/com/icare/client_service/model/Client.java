package com.icare.client_service.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by ahadri on 1/19/17.
 */
@Entity
public class Client {

	@Id
	@GeneratedValue
	private Long id;

	private String nom;
	private String prenom;
	private String cin;
	private String telephone;

	@Override
	public String toString() {
		return "Client{" +
					   "id=" + id +
					   ", nom='" + nom + '\'' +
					   ", prenom='" + prenom + '\'' +
					   ", cin='" + cin + '\'' +
					   ", telephone='" + telephone + '\'' +
					   ", email='" + email + '\'' +
					   ", age='" + age + '\'' +
					   '}';
	}

	private String email;
	private String age;


	public Client() {
	}

	public Client(String nom, String prenom, String cin, String telephone, String email, String age) {
		this.nom = nom;
		this.prenom = prenom;
		this.cin = cin;
		this.telephone = telephone;
		this.email = email;
		this.age = age;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getCin() {
		return cin;
	}

	public void setCin(String cin) {
		this.cin = cin;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}
}
