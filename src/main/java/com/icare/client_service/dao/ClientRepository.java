package com.icare.client_service.dao;

import com.icare.client_service.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by ahadri on 1/19/17.
 */

@Repository
 public interface ClientRepository extends JpaRepository<Client,Long> {

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("update Client client set client.nom = :nom, client.prenom = :prenom, client.telephone = :telephone, client.email = :email, client.cin = :cin, client.age = :age where client.id = :id")
	void updateUser(@Param("nom") String nom, @Param("prenom") String prenom,@Param("telephone") String telephone,@Param("email") String email, @Param("cin") String cin,@Param("age") String age, @Param("id") Long id);
}
