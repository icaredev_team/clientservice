package com.icare.client_service;

import com.icare.client_service.dao.ClientRepository;
import com.icare.client_service.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@SpringBootApplication
@RestController
@RequestMapping("/client-service")
@EnableDiscoveryClient
public class ClientServiceApplication {

	@Autowired
	private ClientRepository clientRepository;

	@RequestMapping(method = RequestMethod.GET, value = "/")
	public String howAmI() {
		return "client-service";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/clients")
	public void addClient(@RequestBody  Client client)
	{
		clientRepository.save(client);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/clients")
	public void updateClient(@RequestBody  Client client) {
		clientRepository.updateUser(client.getNom(), client.getPrenom(), client.getTelephone(), client.getEmail(), client.getCin(), client.getAge(), client.getId());
	}

	@RequestMapping(method = RequestMethod.GET, value = "/clients/{clientId}")
	public Client getClient(@PathVariable("clientId") Long id)
	{
		return clientRepository.findOne(id);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/clients")
	public List<Client> getClients() {
		return clientRepository.findAll();
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/clients/{clientId}")
	public void dropClient(@PathVariable("clientId") Long id) {
		clientRepository.delete(id);
	}

	// Launch the application
	public static void main(String[] args) {
		SpringApplication.run(ClientServiceApplication.class, args);
	}
}
